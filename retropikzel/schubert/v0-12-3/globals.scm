(define-library
  (retropikzel schubert v0-12-3 globals)
  (import (scheme base))
  (export slash
          search-index-url)
  (begin

    (define slash (cond-expand (windows (string #\\)) (else "/")))

    (define search-index-url "https://codeberg.org/Schubert/schubert-search-index.git")

    )

  )
