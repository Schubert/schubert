(define-library
  (retropikzel schubert v0-12-3 util)
  (import (scheme base)
          (scheme file)
          (scheme read)
          (scheme process-context)
          (scheme write)
          (retropikzel string-util v1-0-0 main)
          (retropikzel pffi-shell v0-3-2 main)
          (retropikzel schubert v0-12-3 globals))
  (export shell
          ask
          slurp
          git-clone
          git-clone-no-tag
          git-pull
          git-tags
          git-hash
          file-list
          copy-file
          copy-directory-content
          make-directory
          remove-directory)
  (begin

    (define shell
      (lambda (command)
        (pffi-shell command)))

    (define slurp
      (lambda (path)
        (with-input-from-file
          path
          (lambda ()
            (letrec ((looper
                       (lambda (result line)
                         (if (eof-object? line)
                           result
                           (looper (string-append result line (string #\newline))
                                   (read-line))))))
              (looper "" (read-line)))))))

    (define git-binary (string-append (cond-expand (windows "git.exe") (else "git"))))

    (define ask (lambda (question comment)
                  (display question)
                  (display "  ")
                  (display "(")
                  (display comment)
                  (display ")")
                  (display ": ")
                  (flush-output-port)
                  (read-line)))

    (define git-clone
      (lambda (url tag destination)
        (shell (string-append git-binary " clone " url " --branch " tag " " destination))))

    (define git-clone-no-tag
      (lambda (url destination)
        (shell (string-append git-binary " clone " url " " destination))))

    (define git-pull
      (lambda (path)
        (shell (string-append "cd " path " && " git-binary " pull"))))

    (define git-tags
      (lambda (path)
        (shell (string-append "cd " path " && " git-binary " tag --list"))))

    (define git-hash
      (lambda (path tag)
        (shell (string-append git-binary " -C " path " show-ref -s " tag))))

    (define file-list
      (lambda (path)
        (let* ((command (cond-expand (windows "dir /b") (else "ls")))
               (output (shell (string-append "cd " path " && " command))))
          (if (string=? (cdr (assoc 'stderr output)) "")
            (string-util-split-by-char (cdr (assoc 'stdout output)) #\newline)
            (error (cdr (assoc 'stderr output)))))))

    (define copy-file
      (lambda (file-path destination-path)
        (cond-expand
          (windows (shell (string-append "xcopy /s/e /y "
                                         (string-util-replace-all file-path
                                                                  slash
                                                                  (string #\\))
                                         " "
                                         (string-util-replace-all destination-path
                                                                  slash
                                                                  (string #\\)))))
          (else (shell (string-append "cp " file-path " " destination-path))))))

    (define copy-directory-content
      (lambda (source-path destination-path)
        (cond-expand
          (windows
            (shell (string-append "xcopy /s/e /y "
                                  (string-util-replace-all source-path
                                                           slash
                                                           (string #\\))
                                  slash
                                  (string #\*)
                                  " "
                                  (string-util-replace-all destination-path
                                                           slash
                                                           (string #\\))
                                  (string #\\))))
          (else (shell (string-append "cp -r " source-path slash "*" " " destination-path))))))

    (define make-directory
      (lambda (path)
        (cond-expand
          (windows (shell (string-append "mkdir " path)))
          (else (shell (string-append "mkdir -p " path))))))

    (define remove-directory
      (lambda (path)
        (cond-expand
          (windows (shell (string-append "rmdir /s /q " path)))
          (else (shell (string-append "rm -rf " path))))))))
