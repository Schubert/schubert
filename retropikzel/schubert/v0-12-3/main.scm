(define-library
  (retropikzel schubert v0-12-3 main)
  (import (scheme base)
          (scheme file)
          (scheme read)
          (scheme process-context)
          (scheme write)
          (scheme char)
          (retropikzel string-util v1-0-0 main)
          (retropikzel schubert v0-12-3 globals)
          (retropikzel schubert v0-12-3 util))
  (export schubert-help
          schubert-init
          schubert-search
          schubert-install
          schubert-compose
          schubert-repository-init
          schubert-repository-update
          schubert-cache-clean)
  (begin

    (define use-repository
      (lambda (repository schubert-home)
        (display "\tUsing repository: ")
        (display repository)
        (newline)
        (if (not (string=? repository "local"))
          (let ((repository-path (string-append schubert-home slash "repository")))
            (remove-directory repository-path)
            (git-clone-no-tag repository repository-path)))))

    (define check-hash
      (lambda (path tag hash)
        (let* ((output (git-hash path (symbol->string tag)))
               (repository-hash
                 (begin
                   (if (not (= (cdr (assoc 'exit-code output)) 0))
                     (error "Could not get hash for downloaded library" output))
                   (car (string-util-split-by-char (cdr (assoc 'stdout output))
                                                   #\newline)))))
          (if (not (string=? repository-hash hash))
            (begin
              (remove-directory path)
              (error "Downloaded library hash does not match repository versions hash. Removing download."
                     (list (cons 'path path)
                           (cons 'version tag)
                           (cons 'repository-hash repository-hash)
                           (cons 'downloaded-hash hash))))))))

    (define download
      (lambda (packager name version download-cache default-repository schubert-home)
        (display "\tDownloading: ")
        (display (list packager name version))
        (newline)
        (let* ((library-list
                 (string-append schubert-home slash "repository" slash "libraries.scm"))
               (library-list
                 (with-input-from-file library-list (lambda () (read))))
               (meta-path
                 (string-append schubert-home
                                slash
                                "repository"
                                slash
                                "meta"
                                slash
                                (symbol->string packager)
                                slash
                                (symbol->string name)))
               (meta-versions-file-path
                 (string-append meta-path slash "versions.scm"))
               (meta-versions
                 (begin
                   (if (not (file-exists? meta-versions-file-path))
                     (error "No version meta data found for library"
                            (list packager name version)))
                   (with-input-from-file
                     meta-versions-file-path
                     (lambda () (read)))))
               (version-hash #f)
               (library-list-packager
                 (let ((pair (assoc packager library-list)))
                   (if (not pair)
                     (error "Packager not found from library list" packager)
                     (cdr pair))))
               (packager-libraries
                 (let ((pair (assoc 'libraries library-list-packager)))
                   (if (not pair)
                     (error "No libraries found for packager in library list" packager)
                     (cdr pair))))
               (library-data
                 (let ((pair (assoc name packager-libraries)))
                   (if (not pair)
                     (error "Library not found from library list" name)
                     (cdr pair))))
               (url
                 (cdr (assoc 'url library-data)))
               (description
                 (cdr (assoc 'description library-data)))
               (clone-path
                 (string-append download-cache
                                slash
                                (symbol->string packager)
                                "-"
                                (symbol->string name)
                                "-"
                                (symbol->string version))))
          (for-each
            (lambda (meta-version)
              (cond
                ((string=? (car meta-version) (symbol->string version))
                 (set! version-hash (cdr meta-version)))))
            meta-versions)
          (if (not version-hash)
            (error "No such version found for library" (list packager name version)))
          (cond
            ((not (file-exists? clone-path))
             (let ((clone-output (git-clone url (symbol->string version) clone-path)))
               (if (not (= (cdr (assoc 'exit-code clone-output)) 0))
                 (error "Could not clone library" clone-output))
               (check-hash clone-path version version-hash)))))))

    (define schubert-compose
      (lambda (composition-path download-cache library-cache default-repository schubert-home)
        (display "Composing: ")
        (display composition-path)
        (newline)
        (let* ((composition (with-input-from-file composition-path (lambda () (read))))
               (dependencies-with-repositories (cdr (assoc 'dependencies composition))))
          (for-each
            (lambda (dependencies-with-repository)
              (if (not (null? dependencies-with-repository))
                (let ((repository (car dependencies-with-repository))
                      (dependencies (cdr dependencies-with-repository)))
                  (for-each
                    (lambda (dependency)
                      (if (not (null? dependency))
                        (begin
                          (display "\tInstalling: ")
                          (display dependency)
                          (display " into ./schubert")
                          (newline)
                          (let ((download-path
                                  (string-append download-cache
                                                 slash
                                                 (string-util-join (map symbol->string dependency) "-")))
                                (library-cache-path
                                  (string-append library-cache
                                                 slash
                                                 (string-util-join (map symbol->string dependency) slash)))
                                (project-install-path
                                  (string-append "." slash "schubert" slash
                                                 (string-util-join (map symbol->string dependency) slash))))
                            (if (not (file-exists? library-cache-path))
                              (begin
                                (apply download (append dependency (list download-cache default-repository schubert-home)))
                                (schubert-install (string-append download-path slash "composition.scm") schubert-home)))
                            (remove-directory project-install-path)
                            (make-directory project-install-path)
                            (copy-directory-content library-cache-path project-install-path)
                            (schubert-compose (string-append project-install-path slash "composition.scm")
                                              download-cache
                                              library-cache
                                              default-repository
                                              schubert-home)))))
                    dependencies))))
            dependencies-with-repositories))))

    (define schubert-install
      (lambda (composition-path schubert-home)
        (let* ((composition (with-input-from-file composition-path (lambda () (read))))
               (packager (cdr (assoc 'packager composition)))
               (name (cdr (assoc 'name composition)))
               (version (cdr (assoc 'version composition)))
               (base-path (string-util-strip-suffix composition-path "composition.scm"))
               (library-path (string-append base-path
                                            packager
                                            slash
                                            name
                                            slash
                                            version))
               (install-path (string-append schubert-home
                                            slash
                                            "libraries"
                                            slash
                                            packager
                                            slash
                                            name
                                            slash
                                            version)))
          (display "Installing: ")
          (display (list packager name version))
          (display " into ")
          (display install-path)
          (newline)
          (make-directory install-path)
          (copy-file composition-path install-path)
          (copy-directory-content library-path install-path))))

    (define schubert-help
      (lambda ()
        (display "Usage: schubert [OPTION]")
        (newline)
        (newline)

        (display "  ")
        (display "init")
        (newline)
        (display "    ")
        (display "Initialises new project, writes composition.scm file and creates basic structure.")
        (newline)
        (newline)

        (display "  ")
        (display "search")
        (newline)
        (display "    ")
        (display "Search for libraries from search index: https://codeberg.org/Schubert/schubert-search-index")
        (newline)
        (newline)

        (display "  ")
        (display "install")
        (newline)
        (display "    ")
        (display "Installs the library to the local cache.")
        (newline)
        (newline)

        (display "  ")
        (display "compose")
        (newline)
        (display "    ")
        (display "Installs the dependencis defined in composition.scm from local cache to folder named schubert.")
        (newline)
        (newline)

        (display "  ")
        (display "repository-init")
        (newline)
        (display "    ")
        (display "Initialises empty schubert repository. You need to push changes into git yourself.")
        (newline)
        (newline)

        (display "  ")
        (display "repository-update")
        (newline)
        (display "    ")
        (display "Updates schubert repository in this folder. You need to push changes into git yourself.")
        (newline)
        (newline)

        (display "  ")
        (display "cache-clean")
        (newline)
        (display "    ")
        (display "Clean all caches, if you run into problems try running this and then the other command again.")
        (newline)
        (newline)
        ))

    (define schubert-init
      (lambda ()
        (let* ((packager (ask "Packager?" "Most propably your username"))
               (name (ask "Name?" "Name of the project"))
               (license (ask "License?" "License in short form. MIT/GPL3/..."))
               (overwrite? (if (file-exists? "composition.scm")
                             (string-map char-downcase
                                         (ask "composition.scm allready exists, overwrite?" "y/N"))
                             "y"))
               (version "v0-1-0"))
          (if (string=? overwrite? "y")
            (begin
              (if (file-exists? "composition.scm") (delete-file "composition.scm"))
              (let ((composition
                      (list
                        (cons 'packager packager)
                        (cons 'name name)
                        (cons 'version "v0-1-0")
                        (cons 'description "Remember to change this")
                        (cons 'license license)
                        (cons 'dependencies (list))))
                    (pretty-write-composition
                      (lambda (composition path)
                        (with-output-to-file
                          path
                          (lambda ()
                            (display "(")
                            (write (list-ref composition 0))
                            (display "\n  ")
                            (for-each
                              (lambda (item)
                                (write item)
                                (display "\n  "))
                              (list-tail composition 1))
                            (display ")"))))))
                (pretty-write-composition composition "./composition.scm"))))
          (display "Initing...")
          (newline)
          (let ((main-file-template
                  (lambda (packager version name)
                    (string-append "(define-library\n"
                                   "  (" packager " " name " " version " main)\n"
                                   "  (import (scheme base)\n"
                                   "          (scheme write))\n"
                                   "  (export main)\n"
                                   "  (begin\n"
                                   "    (define main\n"
                                   "      (lambda ()\n"
                                   "        (display \"Hello world\")))))"))))
            (let ((library-directory (string-append packager
                                                    "/"
                                                    name
                                                    "/"
                                                    version)))
              (make-directory library-directory)
              (if (not (file-exists? (string-append library-directory "/main.scm")))
                (with-output-to-file
                  (string-append library-directory "/main.scm")
                  (lambda ()
                    (display (main-file-template packager version name))))))))))

    (define schubert-search-update
      (lambda (search-cache search-index-path)
        (if (file-exists? search-index-path)
          (git-pull search-index-path)
          (git-clone-no-tag search-index-url search-index-path))
        (for-each
          (lambda (repository-url)
            (let* ((repository-name
                     (list-ref
                       (reverse (string-util-split-by-char
                                  (list-ref (string-util-split-by-char repository-url
                                                                       #\.)
                                            1)
                                  #\/))
                       0))
                   (repository-clone-path (string-append search-cache slash repository-name)))
              (if (file-exists? repository-clone-path)
                (git-pull repository-clone-path)
                (git-clone-no-tag repository-url))))
          (with-input-from-file (string-append search-index-path slash "index.scm")
                                (lambda () (read))))))

    (define schubert-search
      (lambda (search-cache query)
        (let ((search-index-path (string-append search-cache slash "index")))
          (schubert-search-update search-cache search-index-path)
          (for-each
            (lambda (repository-url)
              (let* ((repository-name
                       (list-ref
                         (reverse (string-util-split-by-char
                                    (list-ref (string-util-split-by-char repository-url
                                                                         #\.)
                                              1)
                                    #\/))
                         0))
                     (repository-clone-path (string-append search-cache slash repository-name)))
                (display "Searching: ")
                (display repository-name)
                (newline)
                (for-each
                  (lambda (packager-data)
                    (for-each
                      (lambda (library-data)
                        (let ((name (symbol->string (car library-data)))
                              (description (cdr (assoc 'description (cdr library-data)))))
                          (for-each
                            (lambda (keyword)
                              (cond
                                ((or (string-util-contains? name keyword)
                                     (string-util-contains? description keyword))
                                 (display name)
                                 (newline)
                                 (display description)
                                 (newline)
                                 (newline)
                                 )))
                            query)))
                      (cdr (assoc 'libraries (cdr packager-data)))))
                  (with-input-from-file (string-append repository-clone-path slash "libraries.scm")
                                        (lambda () (read))))
                ))
            (with-input-from-file (string-append search-index-path slash "index.scm")
                                  (lambda () (read)))))))

    (define schubert-repository-init
      (lambda ()
        (with-output-to-file
          "libraries.scm"
          (lambda ()
            (write
              (list (list 'example-packager
                          (list 'libraries
                                (list 'example-library
                                      (list (cons 'url "https://codeberg.org/packager/example-library.git")
                                            (cons 'description "This is an example library")))))))))
        (with-output-to-file
          "description.md"
          (lambda ()
            (display "Description of this repository")))
        (make-directory "meta")))

    (define validate-version
      (lambda (version)
        (let ((splitted (string-util-split-by-char version #\-)))
          (and (= (length splitted) 3)
               (char=? (string-ref (list-ref splitted 0) 0) #\v)
               (string->number (string-copy (list-ref splitted 0) 1))
               (string->number (list-ref splitted 1))
               (string->number (list-ref splitted 2))))))

    (define schubert-repository-update
      (lambda (download-cache)
        (let ((libraries-file-content (with-input-from-file "libraries.scm" (lambda () (read))))
              (readme
                (string-append (slurp "description.md")
                               (string #\newline)
                               (string #\newline)
                               "# Libraries"
                               (string #\newline)
                               (string #\newline))))
          (for-each
            (lambda (packager-data)
              (let* ((packager (car packager-data))
                     (libraries (cdr (assoc 'libraries (cdr packager-data)))))
                (for-each
                  (lambda (library-data)
                    (let* ((name (car library-data))
                           (data (cdr library-data))
                           (url (cdr (assoc 'url data)))
                           (description (cdr (assoc 'description data)))
                           (clone-path (string-append download-cache
                                                      slash
                                                      (symbol->string packager)
                                                      "-"
                                                      (symbol->string name)))
                           (clone-result
                             (begin
                               (if (file-exists? clone-path)
                                 (remove-directory clone-path))
                               (git-clone-no-tag url clone-path)))
                           (meta-path
                             (string-append "meta"
                                            slash
                                            (symbol->string packager)
                                            slash
                                            (symbol->string name)))
                           (versions-file-path
                             (string-append meta-path
                                            slash
                                            "versions.scm"))
                           (versions
                             (begin
                               (if (not (file-exists? meta-path))
                                 (make-directory meta-path))
                               (if (not (file-exists? versions-file-path))
                                 (with-output-to-file
                                   versions-file-path
                                   (lambda ()
                                     (write (list)))))
                               (with-input-from-file
                                 versions-file-path
                                 (lambda ()
                                   (read))))))

                      (if (not (= (cdr (assoc 'exit-code clone-result)) 0))
                        (error "Git clone failed" clone-result))
                      (let ((tags (string-util-split-by-char (cdr (assoc 'stdout (git-tags clone-path)))
                                                             #\newline))
                            (validated-tags (list)))

                        (for-each
                          (lambda (tag)
                            (cond
                              ((validate-version tag)
                               (set! validated-tags (append validated-tags (list tag)))
                               (let ((hash (car (string-util-split-by-char (cdr (assoc 'stdout (git-hash clone-path tag))) #\newline)))
                                     (version-exists? #f))
                                 (for-each
                                   (lambda (version)
                                     (let ((file-version (car version)))
                                       (if (string=? file-version tag)
                                         (set! version-exists? #t))))
                                   versions)

                                 (cond ((not version-exists?)
                                        (set! versions (append versions (list (cons tag hash))))
                                        (if (file-exists? versions-file-path)
                                          (delete-file versions-file-path))
                                        (with-output-to-file
                                          versions-file-path
                                          (lambda ()
                                            (write versions)))))))))
                          tags)

                        (set! readme (string-append readme
                                                    "## ("
                                                    (symbol->string packager)
                                                    " "
                                                    (symbol->string name)
                                                    " "
                                                    (car (reverse validated-tags))
                                                    ")"
                                                    (string #\newline)
                                                    (string #\newline)
                                                    description
                                                    (string #\newline)
                                                    (string #\newline)
                                                    "Versions: "
                                                    (apply string-append
                                                           (map (lambda (tag) (string-append tag " "))
                                                                validated-tags))
                                                    (string #\newline)
                                                    (string #\newline)
                                                    (string #\newline)))
                        )))
                  libraries)))
            libraries-file-content)
          (with-output-to-file
            "README.md"
            (lambda ()
              (display readme)
              (newline))))))

    (define schubert-cache-clean
      (lambda (paths)
        (display "Cleaning caches...")
        (newline)
        (for-each
          (lambda (path)
            (remove-directory path))
          paths)
        (display "Caches cleaned")
        (newline)))))
