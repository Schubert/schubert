Project dependency manager for Scheme libraries. Inspired by Composer and NPM.

Please note that Schubert is currently in **alpha** stage.

See the [default repository](https://codeberg.org/Schubert/schubert-repository-r7rs-small) for
R7RS Small libraries.

See the [PFFI repository](https://codeberg.org/r7rs-pffi/schubert-repository-pffi) for libraries
that use the [pffi](https://codeberg.org/r7rs-pffi/pffi).


Got a [bug report](https://codeberg.org/Schubert/schubert/projects/9253)?

Got a [feature suggestions](https://codeberg.org/Schubert/schubert/projects/9252)?

Got a [questions](https://codeberg.org/Schubert/schubert/projects/9254)?


# Installation

## Unix

### Install

- Install [Guile](https://www.gnu.org/software/guile/download/) and [git](https://git-scm.com/)
    - On Debian/Ubuntu run "sudo apt install guile git"
    - On Fedora run "sudo dnf install guile30 git"
- Download the latest [release](https://codeberg.org/Schubert/schubert/releases) unpack it and cd
    into it

Run

    sudo sh install.sh

### Uninstall
Run

    sudo sh uninstall.sh

## Guix

- Install Guile and git
    - guix install guile git

Propably easiest way to install on guix is installing to user directory and then adding that into
your path:

    PREFIX=${HOME}/.local sh install.sh

and then add:

    PATH="${HOME}/.local:$PATH"

into end of your ~/.bashrc file

### Guix note

On guix you need to be in guix shell with glibc to run schubert.

    guix shell glibc

this is unfortunate but it will be fixed at some point.


## Windows

### Install

- Install [Sagittarius](https://bitbucket.org/ktakashi/sagittarius-scheme/downloads/)
    - Please note that it needs to be installed into default location, or be in the PATH for schubert
    to work
- Install [Git](https://git-scm.com/download/win)
    - Pleases note that git needs to be in the path for Schubert to work (it is by default when you
    install it
- Download the latest [release](https://codeberg.org/Schubert/schubert/releases)
- Unpack it
- As user run:
    install.bat
- As admin add the installation folder to path:
    setx /M PATH "%PATH%;%APPDATA%\Schubert\"

### Uninstall

Run:

    uninstall.bat

# Documentation

## Initing

Run

    schubert init

Schubert then will ask you for the packager of library/program, most propably your name/nickname,
the name of the libary/program and the license.

If you started with empty directory, after running this command it's structure should look like
this

    .
    ├── composition.scm
    └── your-name
        └── library-name
            └── v0-1-0
                └── main.scm

and the contents of composition.scm should look like this

    ((packager . "your-name")
      (name . "library-name")
      (version . "v0-1-0")
      (description . "Remember to change this")
      (license . "LIBRARY-LICENSE")
      (dependencies)
                )

## Adding a dependency

To add a dependency into your project add list like this

    ("default"
        (retropikzel string-util v1-0-0))

into your composition.scm under dependencies.

After that your composition.scm should look like this


    ((packager . "your-name")
      (name . "library-name")
      (version . "v0-1-0")
      (description . "Remember to change this")
      (license . "LIBRARY-LICENSE")
      (dependencies
        ("default" (retropikzel string-util v1-0-0)))
                )

the "default" refers to the repository from which the library should be gotten from, well
get to that in detail later.

## Composing your project

After adding your dependency, run

    schubert compose

and let schubert do its thing. After the command has run the directorys structure should look like
this

    .
    ├── composition.scm
    ├── schubert
    │   └── retropikzel
    │       └── string-util
    │           └── v1-0-0
    │               ├── composition.scm
    │               └── main.scm
    └── your-name
        └── library-name
                └── v0-1-0
                    └── main.scm

The dependencies are installed in the "schubert" folder. When running your program you should add
this folder into the load path of your scheme implementation.


## Using the libraries

To use the libraries, import them into your program. Here is an example hello.scm

    (import (scheme base)
            (scheme write)
            (retropikzel string-util v1-0-0 main))


    (write (string-util-split-by-char "Hello world" #\space))
    (newline)

Note that we import the main.scm from retropikzel/string-util/v1-0-0 directory and the import is
different from the dependency definition in composition.scm.

Then run hello.scm

With Guile

    guile -L ./schubert hello.scm

With Sagittarius

    sash -L ./schubert hello.scm


## Working with/installing libraries locally

I find myself often working with two or three libraries and a program that uses them. For this
situtation schubert has a command "install". Which installs the library into local cache, for
schubert compose command to use.

Lets say you have libraries A and B, and program C that uses them. For C to be able to use the
libraries they do not have to be in repository, it is enough to install them locally.

To do this navigate into the library A directory and run

    schubert install

and navigate into the library B directory and run

    shubert install


Your dependencies in program C's composition.scm should look something like this.

    ...
    (dependecies
        ("default"
            (PACKAGER A VERSION)
            (PACKAGER B VERSION)))
    ...

then in program C directory run

    schubert compose

and schubert directory structure should be after that something like this

    .
    └── PACKAGER
        ├── A
        │   └── VERSION
        │       ├── composition.scm
        │       └── main.scm
        └── B
            └── VERSION
                ├── composition.scm
                └── main.scm

you do not need to change the version of your library, running schubert install will overwrite the
library directory in local cache and schubert compose command will overwrite the ibrary directory
under ./schubert



## About the repositories

Each entry under dependencies in composition.scm consist of repository path and list of libraries
from that repository. The repositry "default" refers to schubert
[default repository](https://codeberg.org/Schubert/schubert-repository-r7rs-small) and schubert
switches the value under the hood to "https://codeberg.org/Schubert/schubert-repository-r7rs-small".

To use another repository substitute the "default" with repositorys git url. Like this

    ...
    (dependencies
        ("https://codeberg.org/r7rs-pffi/schubert-repository-pffi.git"
            (retropikzel pffi-shell v0-3-1)))
    ...

Schubert repositories are just git repositories, made with commands schubert repository-init and
updated by schubert repository-update.

## Adding your library to the default repository

Schuberts default repository is
[https://codeberg.org/Schubert/schubert-repository-r7rs-small](https://codeberg.org/Schubert/schubert-repository-r7rs-small)
which hosts libraries that are portable, explained in the repositorys readme.
