((packager . "retropikzel")
  (name . "pffi-shell")
  (version . "v0-3-2")
  (description . "Library to run shell commands")
  (license . "LGPL")
  (dependencies
    ("https://codeberg.org/r7rs-pffi/schubert-repository-pffi.git"
     (retropikzel pffi v0-3-0))))
