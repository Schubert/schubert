(define-library
  (retropikzel pffi-shell v0-3-2 main)
  (import (scheme base)
          (scheme write)
          (scheme file)
          (scheme process-context)
          (retropikzel pffi v0-3-0 main))
  (export pffi-shell)
  (begin

    (define null* (pffi-pointer-null))
    (define slash (cond-expand (windows (string #\\)) (else "/")))

    (cond-expand
      (windows
        (define libkernel (pffi-shared-object-auto-load (list "windows.h"
                                                              "namedpipeapi.h")
                                                        (list)
                                                        "kernel32"
                                                        (list))))
      (else
        (define libc (pffi-shared-object-auto-load (list "unistd.h"
                                                         "stdlib.h")
                                                   (list)
                                                   "c"
                                                   (list ".6")))))
    (cond-expand
      (windows
        (define cmd-path
          (if (get-environment-variable "SystemRoot")
            (string-append (get-environment-variable "SystemRoot") slash "system32" slash "cmd.exe")
            (string-append "C:" slash "windows" slash "system32" slash "cmd.exe")))
        (define startup-info (pffi-pointer-allocate (+ (pffi-size-of 'int)
                                                       (* (pffi-size-of 'pointer) 3)
                                                       (* (pffi-size-of 'int) 8)
                                                       (* (pffi-size-of 'int16) 2)
                                                       (* (pffi-size-of 'pointer) 4))))
        (define process-info (pffi-pointer-allocate (+ (* (pffi-size-of 'pointer) 2)
                                                       (* (pffi-size-of 'int) 2))))
        (define stdout-file (string-append (if (get-environment-variable "TMP")
                                             (get-environment-variable "TMP")
                                             (string-append slash tmp))
                                           slash
                                           "stdout.txt"))
        (define stderr-file (string-append (if (get-environment-variable "TMP")
                                             (get-environment-variable "TMP")
                                             (string-append slash tmp))
                                           slash
                                           "stderr.txt"))
        (pffi-define c-create-process
                     libkernel
                     'CreateProcessA
                     'int
                     (list 'pointer
                           'pointer
                           'pointer
                           'pointer
                           'int
                           'int
                           'pointer
                           'pointer
                           'pointer
                           'pointer))
        (pffi-define c-wait-for-single-object
                     libkernel
                     'WaitForSingleObject
                     'int
                     (list 'pointer 'int))
        (pffi-define c-get-last-error
                     libkernel
                     'GetLastError
                     'int
                     (list)))
      (else
        (pffi-define c-memset libc 'memset 'void (list 'pointer 'int 'int))
        (pffi-define c-fork libc 'fork 'int (list))
        (pffi-define c-pipe libc 'pipe 'int (list 'pointer))
        (pffi-define c-close libc 'close 'int (list 'int))
        (pffi-define c-read libc 'read 'int (list 'int 'pointer 'int))
        (pffi-define c-write libc 'write 'int (list 'int 'pointer 'int))
        (pffi-define c-exec libc 'execl 'int (list 'pointer 'pointer 'pointer 'pointer 'pointer))
        (pffi-define c-dup2 libc 'dup2 'int (list 'int 'int))
        (pffi-define c-wait libc 'wait 'int (list 'pointer))

        (define child-stdout (pffi-pointer-allocate (* (pffi-size-of 'int) 2)))
        (define child-stderr (pffi-pointer-allocate (* (pffi-size-of 'int) 2)))))


    (define pffi-shell
      (lambda (command)
        (cond-expand
          (windows
            (let* ((full-command (string-append "/c "
                                                command
                                                " > "
                                                stdout-file
                                                " 2> "
                                                stderr-file))
                   (exit-code (if (= 0 (c-create-process (pffi-string->pointer cmd-path)
                                                         (pffi-string->pointer full-command)
                                                         null*
                                                         null*
                                                         1
                                                         0
                                                         null*
                                                         null*
                                                         startup-info
                                                         process-info))
                                1
                                0)))
              (c-wait-for-single-object (pffi-pointer-get process-info 'pointer 0) 9999999)
              (list (cons 'exit-code exit-code)
                    (cons 'stdout
                          (with-input-from-file
                            stdout-file
                            (lambda ()
                              (letrec ((looper (lambda (result line)
                                                 (if (eof-object? line)
                                                   result
                                                   (looper (string-append result line (string #\newline))
                                                           (read-line))))))
                                (looper "" (read-line))))))
                    (cons 'stderr
                          (with-input-from-file
                            stderr-file
                            (lambda ()
                              (letrec ((looper (lambda (result line)
                                                 (if (eof-object? line)
                                                   result
                                                   (looper (string-append result line)
                                                           (read-line))))))
                                (looper "" (read-line)))))))))
          (else
            (c-pipe child-stdout)
            (c-pipe child-stderr)
            (let ((pid (c-fork)))
              (cond
                ((= pid 0)
                 (c-close (pffi-pointer-get child-stdout 'int 0))
                 (c-dup2 (pffi-pointer-get child-stdout 'int (pffi-size-of 'int)) 1)
                 (c-dup2 (pffi-pointer-get child-stderr 'int (pffi-size-of 'int)) 2)
                 (c-close (pffi-pointer-get child-stdout 'int (pffi-size-of 'int)))
                 (c-exec (pffi-string->pointer "/bin/sh")
                         (pffi-string->pointer "sh")
                         (pffi-string->pointer "-c")
                         (pffi-string->pointer command)
                         null*))
                (else
                  (letrec* ((buffer-size 1024)
                            (buffer
                              (let ((pointer (pffi-pointer-allocate buffer-size)))
                                (c-memset pointer 0 buffer-size)
                                pointer))
                            (reader
                              (lambda (last-read-count file-descriptor result)
                                (if (= last-read-count 0)
                                  (begin
                                    (c-memset buffer 0 buffer-size)
                                    result)
                                  (let ((read-count (c-read (pffi-pointer-get file-descriptor 'int 0) buffer buffer-size)))
                                    (reader read-count
                                            file-descriptor
                                            (string-append result
                                                           (if (> read-count 0)
                                                             (string-copy (pffi-pointer->string buffer))
                                                             ""))))))))
                    (c-close (pffi-pointer-get child-stdout 'int (pffi-size-of 'int)))
                    (c-close (pffi-pointer-get child-stderr 'int (pffi-size-of 'int)))
                    (let* ((status (pffi-pointer-allocate (pffi-size-of 'int))))
                      (c-wait status)
                      (list (cons 'exit-code (pffi-pointer-get status 'int 0))
                            (cons 'stdout (reader buffer-size child-stdout ""))
                            (cons 'stderr (reader buffer-size child-stderr ""))))))))))))))
