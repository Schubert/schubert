#!/bin/sh

if [ "${PREFIX}" = "" ];
then
    PREFIX=/usr/local
fi

if [ "${IMPLEMENTATION}" = "" ];
then
    echo "1) Guile"
    echo "2) Sagittarius"
    echo ""
    read -p "Choose the implementation you want to run Schubert with(default: 1): " IMPLEMENTATION
fi

if [ "${IMPLEMENTATION}" = "" ];
then
    IMPLEMENTATION=1
fi

cp -r schubert ${PREFIX}/lib/schubert/schubert
cp -r retropikzel/schubert ${PREFIX}/lib/schubert/schubert/retropikzel/
cp -r schubert.scm ${PREFIX}/lib/schubert/schubert.scm
echo "#!/bin/sh" > ${PREFIX}/bin/schubert
echo "export PREFIX=${PREFIX}" >> ${PREFIX}/bin/schubert
echo "export SCHUBERT_HOME=${HOME}/.schubert" >> ${PREFIX}/bin/schubert
echo "export SCHUBERT_CACHE=/tmp/schubert" >> ${PREFIX}/bin/schubert

INCLUDE_PATH="${PREFIX}/lib/schubert/schubert"
SCHUBERT_SCM="${PREFIX}/lib/schubert/schubert.scm"
BIN_FILE="${PREFIX}/bin/schubert"

case "${IMPLEMENTATION}" in
    1)
        echo "guile -L ${INCLUDE_PATH} ${SCHUBERT_SCM} \"\$@\" 2>/dev/null" >> ${BIN_FILE}
        # On some implementations guile 3 binary is guile3.0
        # Check if it exists and if it does then apply fix
        which guile3.0 && sed -i 's/guile/guile3.0/' ${BIN_FILE}
        ;;
    2)
        echo "sash -r7 -L ${INCLUDE_PATH} ${SCHUBERT_SCM} \"\$@\"" >> ${BIN_FILE}
        ;;
    *)
        echo "Implementation: ${IMPLEMENTATION} not supported"
esac
