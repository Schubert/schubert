@echo off

echo 1) Sagittarius
echo:
set /p IMPLEMENTATION=Choose the implementation you want to run Schubert with: 


if not exist "%APPDATA%\Schubert" mkdir "%APPDATA%\Schubert"
echo F | %systemroot%\System32\xcopy /s/E/v/y "%CD%\schubert.scm" "%APPDATA%\Schubert\schubert.scm"
echo D | %systemroot%\System32\xcopy /s/E/v/y/I "%CD%\schubert" "%APPDATA%\Schubert\schubert"
echo D | %systemroot%\System32\xcopy /s/E/v/y/I "%CD%\retropikzel\schubert" "%APPDATA%\Schubert\schubert\retropikzel\schubert"

echo @echo off > "%APPDATA%\Schubert\schubert.bat"
echo SET PREFIX=%APPDATA%\Schubert >> "%APPDATA%\Schubert\schubert.bat"
echo SET SCHUBERT_HOME=%APPDATA%\Schubert >> "%APPDATA%\Schubert\schubert.bat"
echo SET SCHUBERT_CACHE=%TMP%\Schubert >> "%APPDATA%\Schubert\schubert.bat"

IF "%IMPLEMENTATIOn%"=="1" (
    echo "C:\Program Files (x86)\Sagittarius\sash.exe" -r7 -L %APPDATA%\Schubert\schubert %APPDATA%\Schubert\schubert.scm %%* >> "%APPDATA%\Schubert\schubert.bat"
) ELSE (
    echo "Unsupported implementation: %IMPLEMENTATION%"
)
