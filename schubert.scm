(import (scheme base)
        (scheme file)
        (scheme read)
        (scheme process-context)
        (scheme write)
        (scheme char)
        (retropikzel string-util v1-0-0 main)
        (retropikzel schubert v0-12-3 util)
        (retropikzel schubert v0-12-3 globals)
        (retropikzel schubert v0-12-3 main))

(define schubert-home (get-environment-variable "SCHUBERT_HOME"))
(define cache (get-environment-variable "SCHUBERT_CACHE"))
(define search-cache (string-append cache slash "search"))
(define download-cache (string-append cache slash "download"))
(define library-cache (string-append schubert-home slash "libraries"))
(define default-repository "https://codeberg.org/Schubert/schubert-repository-r7rs-small.git")

(make-directory schubert-home)
(make-directory search-cache)
(make-directory download-cache)
(make-directory library-cache)

(define command
  (if (> (length (command-line)) 1)
    (list-ref (command-line) 1)
    "help"))

(cond
  ((string=? command "init")
   (schubert-init))
  ((string=? command "search")
   (if (not (> (length (command-line)) 2))
     (error "Search needs a query"))
   (schubert-search search-cache
                    (list-tail (command-line) 2)))
  ((string=? command "install")
   (schubert-install "composition.scm" schubert-home))
  ((string=? command "compose")
   (schubert-compose "composition.scm"
                     download-cache
                     library-cache
                     default-repository
                     schubert-home))
  ((string=? command "repository-init")
   (schubert-repository-init))
  ((string=? command "repository-update")
   (schubert-repository-update download-cache))
  ((string=? command "cache-clean")
   (schubert-cache-clean (list download-cache
                               library-cache
                               (string-append schubert-home slash "repository"))))
  (else (schubert-help)))



