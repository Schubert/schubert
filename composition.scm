((packager . "retropikzel")
 (name . "schubert")
 (version . "v0-12-3")
 (license . "AGPL")
 (description . "Project dependency manager for Scheme libraries")
 (dependencies
   ("default"
    (retropikzel string-util v1-0-0))
   ("https://codeberg.org/r7rs-pffi/schubert-repository-pffi.git"
    (retropikzel pffi-shell v0-3-2))))
